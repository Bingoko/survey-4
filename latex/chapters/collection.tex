% !TEX root = ../FnTIR2021-Rec.tex

%\section{Beyond Traditional \ac{VAE}-Based Recommendation Methods}
\chapter{Paper collection}
\label{sec:collection}

This section will introduce our paper collection work, the keywords collected, and the preliminary sorting of the collected papers.

\section{Survey Scope}
\label{sub:scope}

Hyperparameter optimization includes many methods. The scope of our paper is to systematically summarize the methods of hyperparameter optimization and the framework that contains these methods.

We apply the following inclusion criteria when collecting papers. If a paper satisfies any one or more of the following criteria, we will include it. The relevant aspects of hyperparameter optimization that we are talking about include hyperparameters, hyperparameter optimization frameworks, and hyperparameter optimization applications.

1)The paper proposes a hyperparameter optimization method, framework/tool. 

2)The paper applies the hyperparameter optimization method to the machine learning model.

3)The paper proposes a dataset or benchmark specifically designed for hyperparameter optimization. 

4)The paper compares the advantages and disadvantages of hyperparameter optimization methods. 

5)The paper summarizes the research directions/existing shortcomings of hyperparameter optimization.

There are many types and variants of hyperparameter optimization methods, we only discuss those methods under our set system, and those special optimization methods with excellent performance will be discussed separately. Besides, for those long-term optimization methods, we will only give their corresponding references and will not introduce them. The description of hyperparameter optimization methods will pay more attention to their process under the current model. 

\section{Paper Collection Methodology}

To collect the papers across different research areas as much
as possible, We use keyword searches one by one on popular scientific databases such as Google Scholar, IEEE Xplore, arxiv, etc. The keywords used for searching are listed below. 

\begin{itemize}
	\setlength{\itemsep}{0pt}
	\setlength{\parsep}{0pt}
	\setlength{\parskip}{0pt}
	\item Grid search + hyperparameter optimization|tuning
	\item Random search + hyperparameter optimization|tuning
	\item Bayesian optimization + hyperparameter optimization|tuning
	\item Evolutionary algorithm + hyperparameter optimization|tuning
	\item Multi-fidelity model + hyperparameter optimization|tuning
	\item Bandit algorithm + hyperparameter optimization|tuning
	\item Machine learning + hyperparameter optimization|tuning
	\item Deep learning + hyperparameter optimization|tuning
	\item Neural network + hyperparameter optimization|tuning
	
\end{itemize}

Since different authors use different terms for their papers, we will snowball every paper we find to include all research results as completely as possible. Papers found in this way will also be considered for inclusion in accordance with the standards set by \ref{sub:scope}. 

Moreover, We will send emails to the authors of the cited papers and ask them to send us other papers that they know are related to hyperparameter optimization but have not been included. We also asked them to check whether our description about their work in the survey was accurate.

\begin{table}
	%\newcommand{\tabincell}[2]{\begin{tabular}{@{}#1@{}}#2\end{tabular}}
	\caption{Paper Query Results}
	\label{tab:keyword}       % Give a unique label	
	% For LaTeX tables use	
	\resizebox{\textwidth}{!}{
		\begin{threeparttable}
			\begin{tabular}{llll}		
				\hline\noalign{\smallskip}		
				Key Words & Hit & Body\tnote{1} & Snowball\tnote{2}\\		
				\noalign{\smallskip}\hline\noalign{\smallskip}		
				Grid Search + hyperparameter optimization & 15 & 5 & 1 \\
				Random Search + hyperparameter optimization & 53 & 15 & 6 \\
				Bayesian optimization + hyperparameter optimization & 41 & 18 & 9 \\
				Population-based algorithm + hyperparameter optimization & 66 & 29 & 12 \\
				Multi-fidelity + hyperparameter optimization & 11 & 6 & 2 \\
				Learning Curve + hyperparameter optimization & 19 & 10 & 5 \\
				Bandit algorithm + hyperparameter optimization & 27 & 14 & 3 \\
				Machine learning + hyperparameter optimization & 38 & 18 & 4\\	
				Deep learning + hyperparameter optimization & 33 & 16 & 3 \\
				Neural network + hyperparameter optimization & 25 & 12 & 3 \\
				Hyperparameter framework & & & 3 \\
				\noalign{\smallskip}\hline\noalign{\smallskip}	
				Query & & & \\
				Snowball & - & - & 46 \\
				Author feedback & & & \\
				Overall & & & 143 \\
				\noalign{\smallskip}\hline
			\end{tabular}
			\begin{tablenotes}
				\footnotesize
				\item[1] Number of papers cited in the paper
				\item[2] Cited papers extracted from other papers citation
			\end{tablenotes}
		\end{threeparttable}}
\end{table}

\section{Collection Result}

Table \ref{tab:keyword} shows the details of paper collection results. These papers come from websites such as Google scholar , arxiv or springer. Keyword search and snowballing resulted in 109 papers across eleven research areas.

Figure \ref{fig:visual} shows the distribution of papers published in
different research venues. Among all the papers, 30.27\% papers are published in Computer Science. 24.30\% papers are published in Mathematics. Additionally, 16.86\% papers are published in Engineering.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{img/visual1}
	\caption{Publication Venue Distribution}
	\label{fig:visual}
\end{figure}

\section{Paper Organisation}
We present the literature review from two aspects:
1) a literature review of the collected papers, 
2) a statistical analysis of the collected papers, datasets, and tools. 
The sections and the corresponding contents are presented in Table \ref{tab:review}.

\begin{table}[h]
	%\newcommand{\tabincell}[2]{\begin{tabular}{@{}#1@{}}#2\end{tabular}}
	\caption{Review Schema}
	\label{tab:review}       % Give a unique label	
	% For LaTeX tables use	
	\resizebox{\textwidth}{!}{
		\begin{threeparttable}
			\begin{tabular}{lll}		
				\hline\noalign{\smallskip}	
				\textbf{Classification} & \textbf{Sec} & \textbf{Topic}\\		
				\noalign{\smallskip}\hline\noalign{\smallskip}
				\tabincell{l}{Summary of parameter and \\ Hyperparameter adjustment methods} & \tabincell{l}{\ref{sec:Initialization}\\ \ref{sec:Optimization}} & \tabincell{l}{Hyperparameter Initialization Method\\ Hyperparameter Optimization Method} \\
				\noalign{\smallskip}\hline\noalign{\smallskip}
				\tabincell{l}{Application of hyperparameter \\ optimization method} & \tabincell{l}{\ref{sec:compare}\\ \ref{sec:Application}} & \tabincell{l}{Comparison of optimization methods\\ Hyper-parameter optimization Application}  \\
				\noalign{\smallskip}\hline\noalign{\smallskip}
				\noalign{\smallskip}\hline\noalign{\smallskip}
				\tabincell{l}{Current Issues, Challenges, \\ and Future Research Directions}& \tabincell{l}{\ref{sec:Benchmarks} \\ \ref{sec:Overfitting} \\ \ref{sec:Scalability} \\ \ref{sec:Continuous}} & \tabincell{l}{Benchmarks and Comparability \\ Overfitting and Generalization \\ Scalability \\ Continuous updating capability}  \\
				\noalign{\smallskip}\hline
			\end{tabular}
		
	\end{threeparttable}}
\end{table}

\paragraph{1) Literature Review.} The papers in our collection are organised and presented from two angles.
Section \ref{sec:mainapproaches} summarizes the methods used in hyper-parameter, and analyzes the advantages and disadvantages of these methods.
Section \ref{sec:othersmethods} describes the application of hyper-parameter optimization methods in traditional machine learning model and deep learning model.

The two aspects have different focuses of ML testing, each of which is a complete organisation of the total collected papers, 
as a single ML testing paper may fit multiple aspects if being viewed from different angles.

\paragraph{2) Statistical Analysis and Summary.}
We analyse and compare the number of research papers on different hyperparameter optimization methods, machine learning structures (classic/deep learning). 
In section \ref{sec:framework} we will discuss those popular open source libraries or frameworks.

The three different angles for presentation of related work as well as the statistical summary, analysis, and comparison,
enable us to observe the research focus, trend, challenges, opportunities, and directions of ML testing. 
These results are presented in Section \ref{sec:futuredirections}.
